import React from "react";
import "./App.css";
import SignIn from "./components/SignIn"
import DashBoard from "./components/DashBoard";

function App() {
  const [state, setstate] = React.useState()
  const setkey =()=>{
    setstate(localStorage.getItem("key"))
  }
  React.useEffect(() => {
   setkey()
  }, )
  return (
    
    <div>
      {state==="true"
        ? <DashBoard/>
        : <SignIn/>
      }
    </div>
     );
}

export default App;
