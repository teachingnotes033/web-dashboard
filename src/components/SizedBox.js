import React from 'react'

function SizedBox({h}) {
    return (  
       <div style={{height:h}}/>
    )
}

export default SizedBox
