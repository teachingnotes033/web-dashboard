import React from "react";
import { db } from "./firebase";
import {
  Paper,
  FormControl,
  InputLabel,
  Radio,
  Grid,
  OutlinedInput,
  FormControlLabel,
  FormLabel,
  RadioGroup,
  Button,
} from "@material-ui/core";
import SizedBox from "./SizedBox";

function QuizComponent() {
  const [quizTitle, setQuizTitle] = React.useState();
  const [quizDesc, setQuizDesc] = React.useState();
  const [quizSize, setQuizSize] = React.useState();
  const [question, setQuestion] = React.useState();
  const [answer, setAnswer] = React.useState();
  const [optionA, setOptionA] = React.useState();
  const [optionB, setOptionB] = React.useState();
  const [optionC, setOptionC] = React.useState();

  const CreateQuiz = () => {
    if (quizTitle && quizDesc && quizSize !== null) {
      var quizDocRef = db.collection("Quiz").doc(quizTitle);
      quizDocRef
        .set({
          quiz_title: quizTitle,
          quiz_size: quizSize,
          quiz_desc: quizDesc,
        })
        .then(function () {
          alert("Quiz successfully created");
        });
    } else {
      alert("Quiz Details are missing");
    }
  };

  const ClearDetails = () => {
    setQuizTitle("");
    setQuizDesc("");
    setQuizSize("");
    NextQuestion()
    alert("Questions added to quiz")
  };


  const UploadQuestion = ()=>{
    db.collection("Quiz").doc(quizTitle).collection('questions').add({
        answer: answer,
        option_a: optionA,
        option_b: optionB,
        option_c: optionC,
        question: question
    })
    .then(function(docRef) {
        console.log("Document written with ID: ", docRef.id);
    })
    .catch(function(error) {
        console.error("Error adding document: ", error);
    });
  }

  const NextQuestion = ()=>{
    setQuestion("")
    setAnswer("")
    setOptionA("")
    setOptionB("")
    setOptionC("")
  }

  return (
    <div>
      <Paper elevation={3} style={{ padding: 12 }}>
        <h2>Quiz Section</h2>
        <SizedBox h={12} />
        <h3>Create new Quiz</h3>
        <SizedBox h={12} />
        <Paper elevation={12} style={{ padding: 12 }}>
          <h3>Enter Quiz details</h3>
          <FormControl fullWidth variant="outlined">
            <InputLabel htmlFor="outlined-adornment-amount">
              Quiz Title
            </InputLabel>
            <OutlinedInput
              id="outlined-adornment-amount"
              value={quizTitle}
              onChange={(event) => {
                setQuizTitle(event.target.value);
              }}
              labelWidth={60}
            />
          </FormControl>
          <SizedBox h={12} />
          <FormControl fullWidth variant="outlined">
            <InputLabel htmlFor="outlined-adornment-amount">
              Quiz Description
            </InputLabel>
            <OutlinedInput
              id="outlined-adornment-amount"
              value={quizDesc}
              onChange={(event) => {
                setQuizDesc(event.target.value);
              }}
              labelWidth={60}
            />
            <SizedBox h={12} />
            <FormControl fullWidth variant="outlined">
              <InputLabel htmlFor="outlined-adornment-amount">
                Quiz Size
              </InputLabel>
              <OutlinedInput
                id="outlined-adornment-amount"
                value={quizSize}
                onChange={(event) => {
                  setQuizSize(event.target.value);
                }}
                labelWidth={60}
              />
            </FormControl>
          </FormControl>
          <SizedBox h={12} />
          <Grid
            container
            direction="row"
            justify="center"
            alignItems="center"
            spacing={5}
          >
            <Grid item>
              <Button variant="contained" color="primary" onClick={CreateQuiz}>
                Create Quiz
              </Button>
            </Grid>
            <Grid item>
              <Button
                variant="contained"
                color="primary"
                onClick={ClearDetails}
              >
                Clear
              </Button>
            </Grid>
          </Grid>
          <SizedBox h={12} />
          <h3>Enter Questions</h3>
          <SizedBox h={12} />
          <Paper elevation={3} style={{ padding: 12 }}>
            <SizedBox h={12} />
            <FormControl fullWidth variant="outlined">
              <InputLabel htmlFor="outlined-adornment-amount">
                Question
              </InputLabel>
              <OutlinedInput
                id="outlined-adornment-amount"
                value={question}
                onChange={(event) => {
                  setQuestion(event.target.value);
                }}
                labelWidth={60}
              />
            </FormControl>
            <SizedBox h={12} />
            <FormControl fullWidth variant="outlined">
              <InputLabel htmlFor="outlined-adornment-amount">
                Option A
              </InputLabel>
              <OutlinedInput
                id="outlined-adornment-amount"
                value={optionA}
                onChange={(event) => {
                  setOptionA(event.target.value);
                }}
                labelWidth={60}
              />
            </FormControl>
            <SizedBox h={12} />
            <FormControl fullWidth variant="outlined">
              <InputLabel htmlFor="outlined-adornment-amount">
                Option B
              </InputLabel>
              <OutlinedInput
                id="outlined-adornment-amount"
                value={optionB}
                onChange={(event) => {
                  setOptionB(event.target.value);
                }}
                labelWidth={60}
              />
            </FormControl>
            <SizedBox h={12} />
            <FormControl fullWidth variant="outlined">
              <InputLabel htmlFor="outlined-adornment-amount">
                Option C
              </InputLabel>
              <OutlinedInput
                id="outlined-adornment-amount"
                value={optionC}
                onChange={(event) => {
                  setOptionC(event.target.value);
                }}
                labelWidth={60}
              />
            </FormControl>
            <SizedBox h={12} />
            <FormControl component="fieldset">
              <FormLabel component="legend">Correct Answer</FormLabel>
              <RadioGroup row aria-label="position" name="position">
                <FormControlLabel
                  value="Option A"
                  control={<Radio color="primary" />}
                  label="Option A"
                  labelPlacement="start"
                  onChange={() => setAnswer(optionA)}
                />
                <FormControlLabel
                  value="Option B"
                  control={<Radio color="primary" />}
                  label="Option B"
                  labelPlacement="start"
                  onChange={() => setAnswer(optionB)}
                />
                <FormControlLabel
                  value="Option C"
                  control={<Radio color="primary" />}
                  label="Option C"
                  labelPlacement="start"
                  onChange={() => setAnswer(optionC)}
                />
              </RadioGroup>
            </FormControl>
            <SizedBox h={12} />
            <Grid
              container
              direction="row"
              justify="center"
              alignItems="center"
              spacing={5}
            >
              <Grid item>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={UploadQuestion}
                >
                  Upload Question
                </Button>
              </Grid>
              <Grid item>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={NextQuestion}
                >
                  Next Question 
                </Button>
              </Grid>
            </Grid>
          </Paper>
        </Paper>
      </Paper>
    </div>
  );
}

export default QuizComponent;
