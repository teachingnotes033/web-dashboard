import React from "react";
import { db } from "./firebase";
import SizedBox from "./SizedBox";
import FileUpload from "./FileUpload";
import TItleCards from './TitleCards'

import {
  Paper,
  Grid,
  makeStyles,
  FormControl,
  InputLabel,
  OutlinedInput,
  Button,
} from "@material-ui/core";

function NotesComponent() {
  const [state, setstate] = React.useState([]);
  const [newCategory, setNewCategory] = React.useState([]);
  const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      margin: "auto",
      maxWidth: 500,
    },
    image: {
      width: 128,
      height: 128,
    },
    img: {
      margin: "auto",
      display: "block",
      maxWidth: "100%",
      maxHeight: "100%",
    },
  }));

  React.useEffect(() => {
    db.collection("Notes")
      .get()
      .then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          setstate((state) => [...state, doc.id]);
        });
      });
  }, []);

  const handleClick = () => {
    db.collection("Notes").doc(newCategory).set({});
    setNewCategory("");
    window.location.reload();
  };

  const classes = useStyles();
  return (
    <div className={classes.root} >
      <Paper className={classes.paper}>
      <h2>Notes Section</h2>
      <h3>Categories</h3>
        <Grid container spacing={2}>
          <Grid item>
            <Grid
              container
              direction="column"
              justify="center"
              alignItems="center"
            >
              <Grid item>
                <div style={{ }}>
                  {state &&
                    state.map((data, index) => {
                      return (
                        <TItleCards data={data} key={index}/>
                      );
                    })}
                  <SizedBox h={12} />
                  <FormControl fullWidth variant="outlined">
                    <InputLabel htmlFor="outlined-adornment-amount">
                      Category
                    </InputLabel>
                    <OutlinedInput
                      id="outlined-adornment-amount"
                      value={newCategory}
                      onChange={(event) => {
                        setNewCategory(event.target.value);
                      }}
                      labelWidth={60}
                    />
                  </FormControl>
                  <SizedBox h={12} />
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={handleClick}
                  >
                    Submit
                  </Button>
                </div>
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={12} sm container>
            <Grid item>
              <FileUpload />
            </Grid>
          </Grid>
        </Grid>
      </Paper>
    </div>
  );
}

export default NotesComponent;
