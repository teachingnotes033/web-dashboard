import React, { useState } from "react";
import { storage } from "./firebase";
import { db } from "./firebase";
import {
  FormControl,
  InputLabel,
  Card,
  OutlinedInput,
  Button,
} from "@material-ui/core";
import SizedBox from "./SizedBox";

export default function ImageUpload() {
  const [image, setImage] = useState(null);
  const [progress, setProgress] = useState(0);
  const [error, setError] = useState("");
  const [category, setCategory] = useState();

  const handleChange = (e) => {
    const file = e.target.files[0];
    if (file) {
      setImage(file);
    }
  };

  const handleUpload = () => {
    if (image) {
      var fileName = image.name.replace(/\s/g, "");
      console.log("file name", fileName);
      const uploadTask = storage.ref(`${fileName}`).put(image);
      uploadTask.on(
        "state_changed",
        (snapshot) => {
          const progress = Math.round(
            (snapshot.bytesTransferred / snapshot.totalBytes) * 100
          );
          setProgress(progress);
        },
        (error) => {
          console.log(error);
          setError(error);
          alert("Document Not Loaded");
        },
        () => {
          storage
            .ref()
            .child(image.name) // Upload the file and metadata
            .getDownloadURL() // get download url
            .then((url) => {
              console.log(url);
              db.collection("Notes")
                .doc(category)
                .collection("notes")
                .doc()
                .set({
                  note_title: image.name,
                  note_url: url,
                })
                .then(function () {
                  console.log("Document successfully written!");
                })
                .catch(function (error) {
                  console.error("Error writing document: ", error);
                });
              setProgress(0);
              alert("Document Loaded Successfully");
            });
        }
      );
    } else {
      setError("Error please choose an image to upload");
    }
  };

  return (
    <div>
      <Card  elevation={8}>
        <br />
        <div style={{padding:12}}>
          <h3>Upload Notes</h3>
          <FormControl fullWidth variant="outlined">
            <InputLabel htmlFor="outlined-adornment-amount">
              Notes Category
            </InputLabel>
            <OutlinedInput
              id="outlined-adornment-amount"
              value={category}
              onChange={(event) => {
                setCategory(event.target.value);
              }}
              labelWidth={60}
            />
          </FormControl>
          <SizedBox h={12} />
          <input type="file" onChange={handleChange} />
          <Button variant="contained" onClick={handleUpload} color="primary">
            Upload
          </Button>
          <br />
        </div>
        <div style={{ height: 14 }}>
          <p style={{ color: "red" }}>{error}</p>
          {progress > 0 ? <progress value={progress} max="100" /> : ""}
        </div>
      </Card>
    </div>
  );
}

// https://firebasestorage.googleapis.com/v0/b/teaching-app-b4a52.appspot.com/o/Datasheet.pdf?alt=media&token=a6570504-6ae8-4398-aef3-1c262693658f
// https://firebasestorage.googleapis.com/v0/b/teaching-app-b4a52.appspot.com/o/Datasheet.pdf?alt=media&token=a6570504-6ae8-4398-aef3-1c262693658f
// https://firebasestorage.googleapis.com/v0/b/teaching-app-b4a52.appspot.com/o/MQ-4.pdf?alt=media&token=94f768c4-162f-476c-ba69-4485fcc9c51e
// https://firebasestorage.googleapis.com/v0/b/teaching-app-b4a52.appspot.com/o/MQ4.pdf?alt=media&token=21290b06-ade3-4600-928f-67db4b36c67e
// https://firebasestorage.googleapis.com/v0/b/teaching-app-b4a52.appspot.com/o/MQ4.pdf?alt=media&token=21290b06-ade3-4600-928f-67db4b36c67e
// https://firebasestorage.googleapis.com/v0/b/teaching-app-b4a52.appspot.com/o/esp32_datasheet_en.pdf?alt=media&token=ee03a8fa-6b84-4c30-b766-fff61f20b6a6
