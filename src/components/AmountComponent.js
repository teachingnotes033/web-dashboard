import React from "react";
import { db } from "./firebase";
import SizedBox from "./SizedBox";
import {
  Paper,
  FormControl,
  InputLabel,
  OutlinedInput,
  Button,
} from "@material-ui/core";

function AmountComponent() {
  const [amount, setAmount] = React.useState();
  const [currentAmount, setCurrentAmount] = React.useState(" ");

  const handleClick = (e) => {
    e.preventDefault();
    var washingtonRef = db.collection("Misc").doc("details");
    if (amount !== "") {
      washingtonRef
        .update({
          amount: parseFloat(amount),
        })
        .then(function () {
          setAmount("");
        })
        .catch(function (error) {
          // The document probably doesn't exist.
          console.error("Error updating document: ", error);
        });
    }
  };

  db.collection("Misc")
    .doc("details")
    .onSnapshot(function (doc) {
      setCurrentAmount(doc.data().amount);
    });

  return (
    <div>
      <Paper elevation={3} style={{ width: 300, padding: 12 }}>
        <h3>The Current Amount is: Rs {currentAmount}</h3>
        <SizedBox h={12} />
        <h4>Enter new Amount here</h4>
        <FormControl fullWidth variant="outlined">
          <InputLabel htmlFor="outlined-adornment-amount">Amount</InputLabel>
          <OutlinedInput
            id="outlined-adornment-amount"
            value={amount}
            onChange={(event) => {
              setAmount(event.target.value);
            }}
            labelWidth={60}
          />
        </FormControl>
        <SizedBox h={12} />
        <Button variant="contained" color="primary" onClick={handleClick}>
          Submit
        </Button>
      </Paper>
    </div>
  );
}

export default AmountComponent;
