import React from "react";
import { db } from "./firebase";
import SizedBox from "./SizedBox";
import {
  Paper,
  FormControl,
  InputLabel,
  OutlinedInput,
  Button,
} from "@material-ui/core";

//UCBJycsmduvYEL83R_U4JriQ

function ChannelComponent(props) {
  const [channel, setchannel] = React.useState();
  const [currentchannel, setCurrentChannel] = React.useState(" ");

  const handleClick = (e) => {
    e.preventDefault();
    var washingtonRef = db.collection("Misc").doc("details");

    if (channel !== "") {
      washingtonRef
        .update({
          channel: channel,
        })
        .then(function () {
          setchannel("");
        })
        .catch(function (error) {
          // The document probably doesn't exist.
          console.error("Error updating document: ", error);
        });
    }
  };
  db.collection("Misc")
    .doc("details")
    .onSnapshot(function (doc) {
      setCurrentChannel(doc.data().channel);
    });
  return (
    <div>
      <Paper elevation={3} style={{ width: 400, padding: 12 }}>
        <h3>The Current Channel Id is:{currentchannel}</h3>
        <SizedBox h={12} />
        <h4>Enter new Youtube Channel Id here</h4>
        <FormControl fullWidth variant="outlined">
          <InputLabel htmlFor="outlined-adornment-amount">Channel Id</InputLabel>
          <OutlinedInput
            id="outlined-adornment-amount"
            value={channel}
            onChange={(event) => {
              setchannel(event.target.value);
            }}
            labelWidth={60}
          />
        </FormControl>
        <SizedBox h={12} />
        <Button variant="contained" color="primary" onClick={handleClick}>
          Submit
        </Button>
      </Paper>
    </div>
  );
}

export default ChannelComponent;
