import React from "react";
import Button from "@material-ui/core/Button";
import AmountComponent from "./AmountComponent";
import SizedBox from "./SizedBox";
import Grid from "@material-ui/core/Grid";
import ChannelComponent from "./ChannelComponent";
import NotesComponent from "./NotesComponent";
import QuizComponent from "./QuizComponent";

function DashBoard() {
    const logOut = ()=>{
        localStorage.setItem("key", "false")
        window.location.reload()
    }
  return (
    <div>
      <SizedBox h={30} />
      <Grid Container direction="column" justify="center" alignItems="center">
        <Grid
          container
          item
          spacing={1}
          direction="row"
          justify="center"
          alignItems="center"
        >
          <Grid item cs={0} sm={3} />
          <Grid item xs={12} sm={3}>
            <AmountComponent />
          </Grid>
          <Grid item xs={12} sm={3}>
            <ChannelComponent />
          </Grid>
          <Grid item cs={0} sm={3} />
        </Grid>
        <SizedBox h={12} />
        <Grid
          item
          container
          direction="row"
          justify="center"
          alignItems="center"
        >
          <Grid item xs={0} sm={3} />
          <Grid item xs={12} sm={6}>
            <NotesComponent />
          </Grid>
          <Grid item xs={0} sm={3} />
        </Grid>
      </Grid>
      <SizedBox h={12} />
      <Grid item container direction="row" justify="center" alignItems="center">
        <Grid item xs={0} sm={3} />
        <Grid item xs={12} sm={6}>
          <QuizComponent />
        </Grid>
        <Grid item xs={0} sm={3} />
      </Grid>
      <SizedBox h={50} />

      <Button variant="contained" color="secondary" onClick={logOut}>
        Log Out
      </Button>
    </div>
  );
}

export default DashBoard;
