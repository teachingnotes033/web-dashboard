import firebase from "firebase";
import "firebase/storage";

const firebaseApp = firebase.initializeApp({
  apiKey: "AIzaSyDLaIcGRFd4AmhHZx2f0YuLdJ0ZUb6B6dM",
  authDomain: "teaching-app-b4a52.firebaseapp.com",
  databaseURL: "https://teaching-app-b4a52.firebaseio.com",
  projectId: "teaching-app-b4a52",
  storageBucket: "teaching-app-b4a52.appspot.com",
  messagingSenderId: "767689679860",
  appId: "1:767689679860:web:6ff0406e7711457f098fa1",
});

const db = firebaseApp.firestore();
const storage = firebase.storage();

export { db,storage };
