import React from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";


function TitleCards(props) {
  return (
    <div style={{padding:12}}>
      <Card style={{  height: 50 }} elevation={10}>
        <CardContent>
          <Typography variant="subtitle1" component="h2">
            {props.data}
          </Typography>
        </CardContent>
      </Card>
    </div>
  );
}

export default TitleCards;
